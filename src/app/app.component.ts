import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
    getUserState,
    UserState,
} from '@eui/core';
import { Observable, Subscription } from 'rxjs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnDestroy {
    userInfos: UserState;
    // Observe state changes
    userState: Observable<UserState>;
    // an array to keep all subscriptions and easily unsubscribe
    subs: Subscription[] = [];
    currentYear: number = new Date().getFullYear();
    getStarted: string;
    sidebarItems = [];
    langChangeSubscription: Subscription;

    // notificationItems = [
    //     { label: 'Title label 1', subLabel: 'Subtitle label' },
    //     { label: 'Title label 2', subLabel: 'Subtitle label' },
    //     { label: 'Title label 3', subLabel: 'Subtitle label' },
    //     { label: 'Title label 4', subLabel: 'Subtitle label' },
    // ];

    constructor(
        private store: Store<any>,
        private translate: TranslateService
    ) {
            this.userState = this.store.select(getUserState);
            this.subs.push(this.userState.subscribe((user: UserState) => {
                this.userInfos = { ...user };
                console.log(user);
            }));

            this.initTranslations();
    }

    initTranslations() {
        this.translate.get('app.get_started').subscribe((res: string) => {
            this.getStarted = res;
            this.sidebarItems = [
                { label: 'Home', url: 'screen/home' },
                { label: 'APIs', url: 'screen/module1', children: [
                    { label: 'Production', url: 'screen/module1/page1' },
                    { label: 'Acceptance', url: 'screen/module1/page2' },
                    { label: 'Intragate', url: 'screen/module1/page2' }
                ] },
                { label: 'Applications', url: 'screen/module2' },
                { label: this.getStarted, url: 'screen/module3' },
            ];
        });


    }

    ngOnDestroy() {
        this.subs.forEach((s: Subscription) => s.unsubscribe());
    }
}
