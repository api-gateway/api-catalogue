# eUI starter app
The EC API Catalogue is a list of all available APIs that you can reuse to make your projects come to fruition much faster.
Feel free to subscribe and use already existing APIs instead of reinventing the wheel.
Also if you desire you can make your APIs available here for others to reuse.

## Features
This EC API Catalogues will feature
 - List all APIs
 - Search through APIs
 - Show the details of an API
 - Subscribe to an API
 - Get the APIs list from different place (on-premise & cloud)
 - Get analytics on the APIs
 - and more
 
 ## Screenshots
 This API Catalogue is being built, here is what it will look like.

**APIs Listing**
![APIs Listing](./static/API_Listing.png)
**APIs Listing Table**
![APIs Listing Table](./static/API_Listing_Table.png)
**API Details**
![API Details](./static/API_Details.png)


# Building the frontend application
## Development server

````npm start```` to start the angular project with node-express proxy mock server

````npm run start-proxy```` to start the angular project with real backend proxy server deployed

````npm run build```` to build, lint and test your project for DEV

````npm run build-prod```` to build, lint and test your project for PROD

````npm run build-prod-skip-test```` to build and lint your project for PROD - Unit test skipped - to be used on Bamboo plans

````npm run build-prod-stats```` to build, lint and test your project for PROD - with stats.json file generated for webpack-bundle-analyzer input

* check package.json for more info on executable scripts provided

## Further help

- https://eui.ecdevops.eu

- register on [MS Teams](https://teams.microsoft.com/l/team/19%3a2f5bb6b7d1e24c4aabaa62229d3e1955%40thread.tacv2/conversations?groupId=fb6def72-c57b-4e8f-a82e-49be65d6e1f5&tenantId=b24c8b06-522c-46fe-9080-70926f8dddb1) with your EC mail account, for extra-muros please send a mail to DIGIT-EUI-SUPPORT@ec.europa.eu

- For bugs / request new features : Drop us an email at : DIGIT-EUI-SUPPORT@ec.europa.eu

